<?php

/**
 * @file
 * View execution hooks for bookkeeping.
 */

use Drupal\views\ViewExecutable;

/**
 * Implements hook_views_pre_build().
 */
function bookkeeping_views_pre_build(ViewExecutable $view) {
  if ($view->id() == 'bookkeeping_batch_export' && isset($view->args[0])) {
    $view->getDisplay()->options['filename'] = "{$view->args[0]}.csv";
  }
}
