<?php

namespace Drupal\bookkeeping\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the account entity.
 *
 * @ConfigEntityType(
 *   id = "bookkeeping_account",
 *   label = @Translation("Account"),
 *   label_singular = @Translation("account"),
 *   label_plural = @Translation("accounts"),
 *   label_collection = @Translation("Chart of Accounts"),
 *   label_count = @PluralTranslation(
 *     singular = "@count account",
 *     plural = "@count accounts",
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\bookkeeping\AccountListBuilder",
 *     "form" = {
 *       "add" = "Drupal\bookkeeping\Form\AccountForm",
 *       "edit" = "Drupal\bookkeeping\Form\AccountForm",
 *       "delete" = "Drupal\bookkeeping\Form\AccountDeleteForm"
 *     },
 *     "access" = "Drupal\bookkeeping\AccountAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "bookkeeping_account",
 *   admin_permission = "administer bookkeeping",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/bookkeeping/accounts/{bookkeeping_account}",
 *     "add-form" = "/admin/bookkeeping/accounts/add",
 *     "edit-form" = "/admin/bookkeeping/accounts/{bookkeeping_account}/edit",
 *     "delete-form" = "/admin/bookkeeping/accounts/{bookkeeping_account}/delete",
 *     "collection" = "/admin/bookkeeping/accounts"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "type",
 *     "rollup",
 *     "code",
 *     "department",
 *   }
 * )
 */
class Account extends ConfigEntityBase implements AccountInterface {

  /**
   * The Account ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Account label.
   *
   * @var string
   */
  protected $label;

  /**
   * The type of account.
   *
   * @var string
   */
  protected $type;

  /**
   * Whether to roll up transactions in exports.
   *
   * @var bool
   */
  protected $rollup = FALSE;

  /**
   * The account code.
   *
   * @var string
   */
  protected $code = '';

  /**
   * The account department.
   *
   * @var string
   */
  protected $department = '';

  /**
   * The account type options.
   *
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup[]
   */
  private static $typeOptions;

  /**
   * {@inheritdoc}
   */
  public function setLabel(string $label) {
    $this->label = $label;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getType(): ?string {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function getTypeLabel(): TranslatableMarkup {
    return self::getTypeOptions()[$this->type];
  }

  /**
   * {@inheritdoc}
   */
  public function setType(string $type) {
    $this->type = $type;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function shouldRollup(): bool {
    return $this->rollup;
  }

  /**
   * {@inheritdoc}
   */
  public function setRollup(bool $rollup) {
    $this->rollup = $rollup;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCode(): string {
    return $this->code;
  }

  /**
   * {@inheritdoc}
   */
  public function setCode(string $code) {
    $this->code = $code;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDepartment(): string {
    return $this->department;
  }

  /**
   * {@inheritdoc}
   */
  public function setDepartment(string $department) {
    $this->department = $department;
    return $this;
  }

  /**
   * Get the options for the account type.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]
   *   The account type labels, keyed by value.
   */
  public static function getTypeOptions(): array {
    // Use static caching for speed.
    if (!isset(self::$typeOptions)) {
      self::$typeOptions = [
        AccountInterface::TYPE_ASSET => new TranslatableMarkup('Asset'),
        AccountInterface::TYPE_LIABILITY => new TranslatableMarkup('Liability'),
        AccountInterface::TYPE_INCOME => new TranslatableMarkup('Income'),
        AccountInterface::TYPE_EXPENSE => new TranslatableMarkup('Expense'),
      ];
    }
    return self::$typeOptions;
  }

  /**
   * {@inheritdoc}
   *
   * Sort order:
   * - Account Type: As per ::getTypeOptions then by type value ascending.
   * - Account code ascending.
   * - Label ascending.
   * - ID ascending.
   */
  public static function sort(ConfigEntityInterface $a, ConfigEntityInterface $b) {
    /** @var self $a */
    /** @var self $b */

    // First order by the account type. We use the order of the items in the
    // type options as their desired order. If the account type isn't in the
    // options, count it as high weighting.
    $type_order = array_flip(array_keys(self::getTypeOptions()));
    $result = ($type_order[$a->type] ?? 99) <=> ($type_order[$b->type] ?? 99);
    if ($result !== 0) {
      return $result;
    }

    // If both were missing account type, sort by the account type.
    if (!isset($type_order[$a->type])) {
      $result = $a->type <=> $b->type;
      if ($result !== 0) {
        return $result;
      }
    }

    // Next sort by code. No code counts as a high weighting.
    if ($a->code xor $b->code) {
      return $a->code ? -1 : 1;
    }
    $result = $a->code <=> $b->code;
    if ($result !== 0) {
      return $result;
    }

    // Next sort by label.
    $result = strnatcasecmp($a->label(), $b->label());
    if ($result !== 0) {
      return $result;
    }

    // Finally sort by ID which is, by definition, always different.
    return strnatcmp($a->id, $b->id);
  }

}
