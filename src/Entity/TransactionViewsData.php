<?php

namespace Drupal\bookkeeping\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Transaction entities.
 */
class TransactionViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['bookkeeping_transaction']['batch']['filter']['allow empty'] = TRUE;
    $data['bookkeeping_transaction']['created']['field']['id'] = 'bookkeeping_transaction_date';
    $data['bookkeeping_transaction']['created']['sort']['id'] = 'bookkeeping_transaction_date';

    $data['bookkeeping_transaction__entries']['delta']['title'] = $this->t('Entries (delta)');
    $data['bookkeeping_transaction__entries']['delta']['field']['id'] = 'numeric';
    $data['bookkeeping_transaction__entries']['delta']['filter']['id'] = 'numeric';
    $data['bookkeeping_transaction__entries']['delta']['sort']['id'] = 'standard';
    $data['bookkeeping_transaction__entries']['delta']['argument']['id'] = 'numeric';

    unset($data['bookkeeping_transaction__entries']['entries_target_id']['field']);
    $data['bookkeeping_transaction__entries']['entries_target_id']['title'] = $this->t('Entries (account)');

    $data['bookkeeping_transaction__entries']['entries_amount']['field']['id'] = 'numeric';
    $data['bookkeeping_transaction__entries']['entries_amount']['field']['float'] = TRUE;
    $data['bookkeeping_transaction__entries']['entries_amount']['filter']['id'] = 'numeric';
    $data['bookkeeping_transaction__entries']['entries_amount']['sort']['id'] = 'standard';
    $data['bookkeeping_transaction__entries']['entries_amount']['argument']['id'] = 'numeric';

    $data['bookkeeping_transaction__entries']['entries_currency_code']['field']['id'] = 'standard';
    $data['bookkeeping_transaction__entries']['entries_currency_code']['filter']['id'] = 'string';
    $data['bookkeeping_transaction__entries']['entries_currency_code']['sort']['id'] = 'standard';
    $data['bookkeeping_transaction__entries']['entries_currency_code']['argument']['id'] = 'string';

    $data['bookkeeping_transaction__entries']['entries_type']['field']['id'] = 'bookkeeping_entry_type';
    $data['bookkeeping_transaction__entries']['entries_type']['filter']['id'] = 'bookkeeping_entry_type';
    $data['bookkeeping_transaction__entries']['entries_type']['sort']['id'] = 'standard';
    $data['bookkeeping_transaction__entries']['entries_type']['argument']['id'] = 'string';

    $properties = [
      'label' => $this->t('label'),
      'code' => $this->t('code'),
      'department' => $this->t('department'),
    ];
    foreach ($properties as $property => $label) {
      $data['bookkeeping_transaction__entries']['entries_account_' . $property] = [
        'title' => $this->t('Entries (Account @property)', [
          '@property' => $label,
        ]),
        'real field' => 'entries_target_id',
        'field' => [
          'id' => 'bookkeeping_entry_account_property',
          'property' => $property,
        ],
      ];
    }

    $data['bookkeeping_transaction__entries']['entries_account_rollup'] = [
      'title' => $this->t('Entries rollup'),
      'help' => $this->t('A query only field for aggregating entries when the account is set to roll up.'),
      'real field' => 'entries_target_id',
      'sort' => ['id' => 'bookkeeping_entries_account_rollup'],
    ];

    return $data;
  }

}
