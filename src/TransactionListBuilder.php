<?php

namespace Drupal\bookkeeping;

use CommerceGuys\Intl\Formatter\CurrencyFormatterInterface;
use Drupal\bookkeeping\Form\TransactionListFilterForm;
use Drupal\bookkeeping\Plugin\Field\FieldType\BookkeepingEntryItem;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Defines a class to build a listing of Transaction entities.
 *
 * @ingroup bookkeeping
 */
class TransactionListBuilder extends EntityListBuilder {

  /**
   * The currency formatter.
   *
   * @var \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface
   */
  protected $currencyFormatter;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('commerce_price.currency_formatter'),
      $container->get('form_builder'),
      $container->get('request_stack'),
      $container->get('date.formatter')
    );
  }

  /**
   * Construct the transaction list builder.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage.
   * @param \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface $currency_formatter
   *   The currency formatter.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, CurrencyFormatterInterface $currency_formatter, FormBuilderInterface $form_builder, RequestStack $request_stack, DateFormatterInterface $date_formatter) {
    parent::__construct($entity_type, $storage);
    $this->currencyFormatter = $currency_formatter;
    $this->formBuilder = $form_builder;
    $this->requestStack = $request_stack;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Transaction ID');
    $header['created'] = $this->t('Date');
    $header['batch'] = $this->t('Batch');
    $header['description'] = $this->t('Description / Account');
    $header['debit'] = $this->t('Debit');
    $header['credit'] = $this->t('Credit');
    $header['total'] = '';
    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\bookkeeping\Entity\Transaction $entity */
    $row['id'] = $entity->id();
    $row['created'] = $this->dateFormatter->format($entity->getCreatedTime(), 'short');
    $row['batch'] = $entity->get('batch')->value;
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.bookkeeping_transaction.canonical',
      ['bookkeeping_transaction' => $entity->id()]
    );
    $total = $entity->getTotal();
    $row['amount'] = [
      'colspan' => 3,
      'class' => ['amount'],
      'data' => [
        '#markup' => $this->currencyFormatter->format($total->getNumber(), $total->getCurrencyCode()),
      ],
    ];
    return $row;
  }

  /**
   * Build the row for a specific entry.
   *
   * @param \Drupal\bookkeeping\Plugin\Field\FieldType\BookkeepingEntryItem $item
   *   The entry item.
   *
   * @return array
   *   The row render array.
   */
  protected function buildEntryRow(BookkeepingEntryItem $item) {
    $formatted_amount = $this->currencyFormatter
      ->format($item->amount, $item->currency_code);
    $account = $item->entity;
    return [
      'id' => ['colspan' => 3],
      'name' => [
        'class' => ['account'],
        'data' => ['#markup' => $account ? $account->label() : $item->target_id],
      ],
      'debit' => [
        'class' => ['amount'],
        'data' => [
          '#markup' => $item->type == BookkeepingEntryItem::TYPE_DEBIT ? $formatted_amount : '',
        ],
      ],
      'credit' => [
        'class' => ['amount'],
        'data' => [
          '#markup' => $item->type == BookkeepingEntryItem::TYPE_CREDIT ? $formatted_amount : '',
        ],
      ],
      'total' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build['form'] = $this->formBuilder->getForm(TransactionListFilterForm::class);

    $build['table'] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#title' => $this->getTitle(),
      '#rows' => [],
      '#empty' => $this->t('There are no @label yet.', ['@label' => $this->entityType->getPluralLabel()]),
      '#cache' => [
        'contexts' => $this->entityType->getListCacheContexts(),
        'tags' => $this->entityType->getListCacheTags(),
      ],
      '#attributes' => [
        'class' => ['bookkeeping-transactions'],
      ],
      '#attached' => [
        'library' => ['bookkeeping/list-builder'],
      ],
    ];
    foreach ($this->load() as $entity) {
      if ($row = $this->buildRow($entity)) {
        $build['table']['#rows'][$entity->id()] = [
          'class' => ['transaction'],
          'data' => $row,
        ];

        foreach ($entity->get('entries') as $delta => $item) {
          $item_row = $this->buildEntryRow($item);
          $build['table']['#rows']["{$entity->id()}:{$delta}"] = [
            'class' => ['entry'],
            'data' => $item_row,
          ];
        }
      }
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $build['pager'] = [
        '#type' => 'pager',
      ];
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->accessCheck()
      ->sort('created', 'DESC')
      ->sort('id', 'DESC');

    $filters = $this->requestStack->getCurrentRequest()->query;

    if ($description = $filters->get('description')) {
      $query->condition('description', $description, 'CONTAINS');
    }

    if ($batch = $filters->get('batch')) {
      $query->condition('batch', $batch, 'CONTAINS');
    }

    if ($accounts = $filters->get('accounts')) {
      $query->condition('entries.target_id', $accounts, 'IN');
    }

    $date = $filters->all('date') ?? [];
    if (!empty($date['from'])) {
      $from = (new DrupalDateTime($date['from']))->setTime(0, 0, 0);
      $query->condition('created', $from->getTimestamp(), '>=');
    }
    if (!empty($date['to'])) {
      $to = (new DrupalDateTime($date['to']))->setTime(23, 59, 59);
      $query->condition('created', $to->getTimestamp(), '<=');
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }

    return $query->execute();
  }

}
