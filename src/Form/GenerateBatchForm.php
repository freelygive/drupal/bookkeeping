<?php

namespace Drupal\bookkeeping\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Bookkeeping form.
 */
class GenerateBatchForm extends FormBase {

  /**
   * The lock name for batch generation.
   */
  const LOCK_NAME = 'bookkeeping.generate_batch';

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The lock backend.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $form = new static(
      $container->get('datetime.time'),
      $container->get('entity_type.manager'),
      $container->get('lock.persistent')
    );
    $form->setRequestStack($container->get('request_stack'));
    $form->setMessenger($container->get('messenger'));
    $form->setLoggerFactory($container->get('logger.factory'));
    return $form;
  }

  /**
   * Construct the batch generation form.
   *
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The persistent lock.
   */
  public function __construct(TimeInterface $time, EntityTypeManagerInterface $entity_type_manager, LockBackendInterface $lock) {
    $this->time = $time;
    $this->entityTypeManager = $entity_type_manager;
    $this->lock = $lock;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bookkeeping_generate_batch';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['actions'] = [
      '#type' => 'actions',
    ];

    $query = $this->getRequest()->query;
    if ($query->has('destination')) {
      $options = UrlHelper::parse($query->get('destination'));
      try {
        $url = Url::fromUserInput('/' . ltrim($options['path'], '/'), $options);
      }
      catch (\InvalidArgumentException $e) {
        // Fall back to the default.
      }
    }

    if (!isset($url)) {
      $url = Url::fromRoute('view.bookkeeping_batches.list');
    }

    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#attributes' => ['class' => ['button']],
      '#url' => $url,
      '#cache' => [
        'contexts' => [
          'url.query_args:destination',
        ],
      ],
      '#weight' => 99,
    ];

    // Check if the lock may be available.
    if (!$this->lock->lockMayBeAvailable(self::LOCK_NAME)) {
      $this->messenger()->addError($this->t('Another batch generation process is already running. This page will refresh when it is finished.'));
      $form['#attached']['http_header'][] = ['Refresh', '5'];
      $form['actions']['cancel']['#title'] = $this->t('Back');
      return $form;
    }

    $max = DrupalDateTime::createFromTimestamp($this->time->getRequestTime())
      ->setTime(23, 59, 59)
      ->sub(new \DateInterval('P1D'));
    $transactions = $this->entityTypeManager
      ->getStorage('bookkeeping_transaction')
      ->getAggregateQuery()
      ->accessCheck()
      ->aggregate('created', 'MIN')
      ->aggregate('id', 'COUNT')
      ->notExists('batch')
      ->condition('created', $max->getTimestamp(), '<=')
      ->execute();

    if (empty($transactions[0]['id_count'])) {
      $this->messenger()->addStatus($this->t('There are no un-exported transaction.'));
      $form['actions']['cancel']['#title'] = $this->t('Back');
      return $form;
    }

    $batch_prefix = $max->format('ymd') . '-';
    $form['batch'] = [
      '#type' => 'number',
      '#title' => $this->t('Batch name'),
      '#description' => $this->t('Batch name must be unique and the suffix is 2 digits. If left blank will be auto generated.'),
      '#field_prefix' => $batch_prefix,
      '#placeholder' => '01',
    ];

    $min_date = DrupalDateTime::createFromTimestamp($transactions[0]['created_min']);
    $min_date->setTime(0, 0, 0);
    $html5_format = DateFormat::load('html_date')->getPattern();

    $form['min'] = [
      '#type' => 'value',
      '#value' => $min_date,
    ];

    $form['max'] = [
      '#type' => 'value',
      '#value' => $max,
    ];

    $form['from'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Transactions from'),
      '#default_value' => $min_date,
      '#date_time_element' => 'none',
      '#attributes' => [
        'min' => $min_date->format($html5_format),
        'max' => $max->format($html5_format),
      ],
    ];

    $form['to'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Transactions to'),
      '#default_value' => $max,
      '#date_time_element' => 'none',
      '#attributes' => [
        'min' => $min_date->format($html5_format),
        'max' => $max->format($html5_format),
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Generate'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Datetime\DrupalDateTime $min */
    $min = $form_state->getValue('min')->setTime(0, 0, 0);
    /** @var \Drupal\Core\Datetime\DrupalDateTime $max */
    $max = $form_state->getValue('max')->setTime(23, 59, 59);

    foreach (['from', 'to'] as $key) {
      $value = $form_state->getValue($key);
      if ($value < $min || $value > $max) {
        $form_state->setError($form[$key], $this->t('%label must be between @min and @max.', [
          '%label' => $form[$key]['#title'],
          '@min' => $min->format($form[$key]['#date_date_format']),
          '@max' => $max->format($form[$key]['#date_date_format']),
        ]));
      }
    }

    if ($batch = $form_state->getValue('batch')) {
      $exists = (bool) $this->entityTypeManager
        ->getStorage('bookkeeping_transaction')
        ->getQuery()
        ->accessCheck()
        ->condition('batch', $this->getBatchPrefix() . $batch)
        ->range(0, 1)
        ->execute();
      if ($exists) {
        $form_state->setError($form['batch'], $this->t('This batch already exists.'));
      }
    }
    else {
      $form_state->setValue('batch', $this->getNextBatchId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!$this->lock->acquire(self::LOCK_NAME)) {
      $form_state->setRedirect('<current>');
      return;
    }

    $batch_id = $this->getBatchPrefix() . $form_state->getValue('batch');
    $batch = (new BatchBuilder())
      ->setTitle($this->t('Generating batch %id', ['%id' => $batch_id]))
      ->setFinishCallback([$this, 'batchFinished'])
      ->setProgressMessage('')
      ->addOperation([$this, 'batchProcess'], [
        $batch_id,
        $form_state->getValue('from')->setTime(0, 0, 0),
        $form_state->getValue('to')->setTime(23, 59, 59),
      ]);
    batch_set($batch->toArray());
    $form_state->setRedirect('view.bookkeeping_batches.list');
  }

  /**
   * Get the batch prefix.
   *
   * @return string
   *   The batch prefix.
   */
  protected function getBatchPrefix() {
    return date('ymd', $this->time->getRequestTime()) . '-';
  }

  /**
   * Get the next available vatch ID.
   *
   * @return string
   *   The batch ID.
   */
  protected function getNextBatchId() {
    // Find the current max batch for today.
    $existing_batch = $this->entityTypeManager
      ->getStorage('bookkeeping_transaction')
      ->getAggregateQuery()
      ->accessCheck()
      ->aggregate('batch', 'MAX')
      ->condition('batch', date('ymd', $this->time->getRequestTime()) . '-', 'STARTS_WITH')
      ->execute();

    // Increment to the next ID.
    if (!empty($existing_batch[0]['batch_max'])) {
      return str_pad(substr($existing_batch[0]['batch_max'], 7) + 1, 2, '0', STR_PAD_LEFT);
    }

    // Otherwise start from 1.
    return '01';
  }

  /**
   * Batch process callback for generating a batch.
   *
   * @param string $batch_id
   *   The batch ID.
   * @param \Drupal\Core\Datetime\DrupalDateTime $from
   *   The from date.
   * @param \Drupal\Core\Datetime\DrupalDateTime $to
   *   The to date.
   * @param array $context
   *   The context for the batch.
   */
  public function batchProcess(string $batch_id, DrupalDateTime $from, DrupalDateTime $to, array &$context) {
    $storage = $this->entityTypeManager->getStorage('bookkeeping_transaction');
    $query = $storage->getQuery()
      ->accessCheck(FALSE)
      ->notExists('batch')
      ->condition('created', $from->getTimestamp(), '>=')
      ->condition('created', $to->getTimestamp(), '<=');

    if (!isset($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['errors'] = 0;
      $context['sandbox']['last_id'] = 0;
      $context['sandbox']['max'] = (clone $query)->count()->execute();
      $context['results'] = [
        'batch_id' => $batch_id,
        'count' => 0,
        'errors' => 0,
      ];
    }

    $query->range(0, 20);
    $query->condition('id', $context['sandbox']['last_id'], '>');
    $query->sort('id', 'ASC');
    $ids = $query->execute();
    /** @var \Drupal\bookkeeping\Entity\TransactionInterface $transaction */
    foreach ($storage->loadMultiple($ids) as $transaction) {
      try {
        $transaction->set('batch', $batch_id);
        $transaction->save();
        $context['results']['count']++;
      }
      catch (\Throwable $throwable) {
        $error = Error::decodeException($throwable);
        $this->getLogger('bookkeeping')->error('%type: @message in %function (line %line of %file) @backtrace_string.', $error);
        $context['results']['errors']++;
      }
      finally {
        $context['sandbox']['progress']++;
        $context['sandbox']['last_id'] = $transaction->id();
      }
    }

    // Release and re-acquire the lock.
    $this->lock->release(self::LOCK_NAME);
    $this->lock->acquire(self::LOCK_NAME);

    $context['message'] = $this->t('Processed @current out of @total transactions.', [
      '@current' => $context['sandbox']['progress'],
      '@total' => $context['sandbox']['max'],
    ]);
    if (!empty($ids)) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Batch finish callback.
   *
   * @param bool $success
   *   Whether the batch succeeded.
   * @param array $results
   *   The results from the batch.
   * @param mixed $operation
   *   The unfinished operations if not successful.
   *
   * @see \callback_batch_finished()
   */
  public function batchFinished(bool $success, array $results, $operation) {
    if (!empty($results['count'])) {
      $this->messenger()
        ->addStatus($this->t('Batch %id generated with %count transactions.', [
          '%id' => $results['batch_id'],
          '%count' => $results['count'],
        ]));
    }

    if (!empty($results['errors'])) {
      $this->messenger()
        ->addWarning($this->t('%errors transactions skipped due to errors. Please see the logs for more details.', [
          '%errors' => $results['errors'],
        ]));
    }

    if (!$success) {
      $this->messenger()
        ->addWarning($this->t('The batch generation was interrupted. You may need to generate an additional batch with the remaining transactions.'));
    }

    $this->lock->release(self::LOCK_NAME);
  }

}
