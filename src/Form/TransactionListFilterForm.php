<?php

namespace Drupal\bookkeeping\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a Bookkeeping form.
 */
class TransactionListFilterForm extends FormBase {

  /**
   * The account entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $accountStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('entity_type.manager')->getStorage('bookkeeping_account')
    );
  }

  /**
   * Construct the transaction filter form.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Entity\EntityStorageInterface $account_storage
   *   The account entity storage.
   */
  public function __construct(RequestStack $request_stack, EntityStorageInterface $account_storage) {
    $this->setRequestStack($request_stack);
    $this->accountStorage = $account_storage;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bookkeeping_transaction_list_filter';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = $this->getRequest();
    $form['#attributes']['class'][] = 'form--inline';

    $form['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#size' => 30,
      '#default_value' => $request->query->get('description'),
    ];

    $form['batch'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Batch'),
      '#size' => 20,
      '#default_value' => $request->query->get('batch'),
    ];

    $account_ids = $request->query->get('accounts');
    $form['accounts'] = [
      '#title' => $this->t('Accounts'),
    ];
    $account_count = $this->accountStorage->getQuery()->accessCheck()->count()->execute();
    if ($account_count > 20) {
      $form['accounts'] += [
        '#type' => 'entity_autocomplete',
        '#tags' => TRUE,
        '#target_type' => 'bookkeeping_account',
        '#default_value' => $account_ids ? $this->accountStorage->loadMultiple($account_ids) : NULL,
        '#size' => 60,
        '#description' => $this->t('You can search and select multiple accounts by separating with a comma.'),
      ];
    }
    else {
      $form['accounts'] += [
        '#type' => 'select',
        '#multiple' => TRUE,
        '#default_value' => $account_ids ?? [],
      ];
      foreach ($this->accountStorage->loadMultiple() as $account) {
        $form['accounts']['#options'][$account->id()] = $account->label();
      }
      // Make the size slightly fluid depending on the number of options.
      // Show all for less than 5.
      if ($account_count < 5) {
        $form['accounts']['#size'] = $account_count;
      }
      elseif ($account_count < 20) {
        $form['accounts']['#size'] = ceil($account_count / 2);
      }
      else {
        $form['accounts']['#size'] = 10;
      }
    }

    $today = new DrupalDateTime();
    $html5_format = DateFormat::load('html_date')->getPattern();
    $form['date'] = ['#tree' => TRUE];
    $date_filter = $request->query->all('date') ?? [];
    $form['date']['from'] = [
      '#type' => 'date',
      '#title' => $this->t('From'),
      '#default_value' => $date_filter['from'] ?? NULL,
      '#attributes' => [
        'type' => 'date',
        'max' => $today->format($html5_format),
      ],
    ];
    $form['date']['to'] = [
      '#type' => 'date',
      '#title' => $this->t('To'),
      '#default_value' => $date_filter['to'] ?? NULL,
      '#attributes' => [
        'type' => 'date',
        'max' => $today->format($html5_format),
      ],
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply'),
    ];

    $form['actions']['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset'),
      '#submit' => ['::resetForm'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $options = [];
    foreach (Element::children($form) as $child) {
      $type = $form[$child]['#type'] ?? FALSE;
      if ($type == 'entity_autocomplete') {
        foreach ($form_state->getValue($child) as $item) {
          $options['query'][$child][] = $item['target_id'];
        }
      }
      elseif (!in_array($type, ['actions', 'hidden', 'token'])) {
        $options['query'][$child] = $form_state->getValue($child);
      }
    }
    $form_state->setRedirect('<current>', [], $options);
  }

  /**
   * {@inheritdoc}
   */
  public function resetForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('<current>');
  }

}
