<?php

namespace Drupal\bookkeeping\Plugin\EntityReferenceSelection;

use Drupal\bookkeeping\Entity\Account;
use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin description.
 *
 * @EntityReferenceSelection(
 *   id = "default:bookkeeping_account",
 *   label = @Translation("Account selection"),
 *   group = "default",
 *   entity_types = {"bookkeeping_account"},
 *   weight = 1
 * )
 */
class AccountSelection extends DefaultSelection {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $default_configuration = [
      'types' => NULL,
    ];
    return $default_configuration + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Account types'),
      '#options' => Account::getTypeOptions(),
      '#default_value' => (array) $this->configuration['types'],
      '#element_validate' => [[get_class($this), 'elementValidateFilter']],
      '#description' => $this->t('If no accounts are selected, all types will be selectable.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = parent::buildEntityQuery($match, $match_operator);

    if (!empty($this->configuration['types'])) {
      $query->condition('type', $this->configuration['types'], 'IN');
    }

    return $query;
  }

}
