<?php

namespace Drupal\bookkeeping\Plugin\views\sort;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\Plugin\views\sort\SortPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides entries account rollup field handler.
 *
 * @ViewsSort("bookkeeping_entries_account_rollup")
 */
class EntriesAccountRollup extends SortPluginBase {

  /**
   * The bookkeeping account entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $accountStorage;

  /**
   * Constructs a new EntryAccountPropertyField instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->accountStorage = $entity_type_manager->getStorage('bookkeeping_account');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();

    // Get the account IDs that should be rolled up.
    $ids = $this->accountStorage->getQuery()
      ->accessCheck()
      ->condition('rollup', TRUE)
      ->execute();
    $count = $this->accountStorage->getQuery()->accessCheck()->count()->execute();

    // Always sort by the account. If all accounts are rolled up, this is all
    // we need.
    $this->query->addOrderBy($this->tableAlias, $this->realField, $this->options['order']);

    // If we have no IDs to roll up, we can simply sort.
    if (empty($ids)) {
      $this->query->addOrderBy($this->tableAlias, 'entity_id', $this->options['order']);
      $this->query->addOrderBy($this->tableAlias, 'delta', $this->options['order']);
    }
    // Otherwise, if there are a mix of rollup and non-rollup, we need to sort
    // using an expression that combines the entity ID and delta to ensure each
    // entry is on a separate row.
    elseif (count($ids) != $count) {
      $placeholder = $this->query->placeholder() . '[]';
      $expression = "IF ({$this->tableAlias}.{$this->realField} IN ({$placeholder}), NULL, CONCAT({$this->tableAlias}.entity_id, '-', {$this->tableAlias}.delta))";
      $params['placeholders'][$placeholder] = $ids;
      $this->query->addOrderBy(NULL, $expression, $this->options['order'], 'bookkeeping_transaction__entries_rollup', $params);
    }
  }

}
