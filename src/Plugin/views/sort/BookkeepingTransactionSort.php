<?php

namespace Drupal\bookkeeping\Plugin\views\sort;

use Drupal\views\Plugin\views\sort\Date;

/**
 * Timezone safe date sort.
 *
 * @ViewsSort("bookkeeping_transaction_date")
 *
 * @see https://www.drupal.org/project/drupal/issues/3181657
 */
class BookkeepingTransactionSort extends Date {

  /**
   * Creates cross-database SQL dates.
   *
   * @return string
   *   An appropriate SQL string for the db type and field type.
   */
  public function getDateField() {
    return "FROM_UNIXTIME($this->tableAlias.$this->realField)";
  }

}
