<?php

namespace Drupal\bookkeeping\Plugin\views\field;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides entry account property field handler.
 *
 * @ViewsField("bookkeeping_entry_account_property")
 */
class EntryAccountPropertyField extends FieldPluginBase {

  /**
   * The bookkeeping account entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $accountStorage;

  /**
   * A cache of already process values, keyed by account ID.
   *
   * @var string[]
   */
  protected $valueCache = [];

  /**
   * Constructs a new EntryAccountPropertyField instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->accountStorage = $entity_type_manager->getStorage('bookkeeping_account');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $account_id = $this->getValue($values);
    if (!$account_id) {
      return NULL;
    }

    if (!array_key_exists($account_id, $this->valueCache)) {
      /** @var \Drupal\bookkeeping\Entity\AccountInterface $account */
      $account = $this->accountStorage->load($account_id);
      if ($account) {
        $value = $account->get($this->definition['property']);
        $this->valueCache[$account_id] = $this->sanitizeValue($value, 'admin_xss');
      }
      else {
        $this->valueCache[$account_id] = NULL;
      }
    }

    return $this->valueCache[$account_id];
  }

}
