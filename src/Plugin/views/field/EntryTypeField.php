<?php

namespace Drupal\bookkeeping\Plugin\views\field;

use Drupal\bookkeeping\Plugin\Field\FieldType\BookkeepingEntryItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Provides the entry type field handler.
 *
 * @ViewsField("bookkeeping_entry_type")
 */
class EntryTypeField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['label_debit'] = ['default' => new TranslatableMarkup('Debit')];
    $options['label_credit'] = ['default' => new TranslatableMarkup('Credit')];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['label_debit'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label for debits'),
      '#default_value' => $this->options['label_debit'],
    ];

    $form['label_credit'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label for credits'),
      '#default_value' => $this->options['label_credit'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $value = $this->getValue($values);
    $type = (int) $value === BookkeepingEntryItem::TYPE_CREDIT ? 'label_credit' : 'label_debit';
    return $this->sanitizeValue($this->options[$type], 'xss_admin');
  }

}
