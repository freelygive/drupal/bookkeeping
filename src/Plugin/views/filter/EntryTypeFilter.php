<?php

namespace Drupal\bookkeeping\Plugin\views\filter;

use Drupal\bookkeeping\Plugin\Field\FieldType\BookkeepingEntryItem;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\InOperator;
use Drupal\views\ViewExecutable;

/**
 * Provides the entry type filter handler.
 *
 * @ViewsFilter("bookkeeping_entry_type")
 */
class EntryTypeFilter extends InOperator {

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, ?array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->valueOptions = [
      BookkeepingEntryItem::TYPE_DEBIT => new TranslatableMarkup('Debit'),
      BookkeepingEntryItem::TYPE_CREDIT => new TranslatableMarkup('Credit'),
    ];
  }

}
