<?php

namespace Drupal\bookkeeping\Plugin\Field\FieldFormatter;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\Exception\UndefinedLinkTemplateException;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\dynamic_entity_reference\Plugin\Field\FieldFormatter\DynamicEntityReferenceFormatterTrait;

/**
 * Plugin implementation of the 'Label & url' formatter.
 *
 * @FieldFormatter(
 *   id = "bookkeeping_related_label_url",
 *   label = @Translation("Label & url"),
 *   field_types = {
 *     "dynamic_entity_reference"
 *   }
 * )
 */
class LabelUrlFormatter extends EntityReferenceFormatterBase {

  use DynamicEntityReferenceFormatterTrait;

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      $label = $entity->label();
      try {
        $uri = $entity->toUrl()
          ->setAbsolute()
          ->toString();
      }
      catch (UndefinedLinkTemplateException $e) {
        $uri = '';
      }

      $elements[$delta] = [
        '#markup' => new FormattableMarkup('@label @url', [
          '@label' => $label,
          '@url' => $uri,
        ]),
      ];
      $elements[$delta]['#cache']['tags'] = $entity->getCacheTags();
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return parent::isApplicable($field_definition) &&
      $field_definition->getTargetEntityTypeId() === 'bookkeeping_transaction' &&
      $field_definition->getName() === 'related';
  }

}
