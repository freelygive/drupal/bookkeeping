<?php

namespace Drupal\bookkeeping\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides a Bookkeeping Entries constraint.
 *
 * @Constraint(
 *   id = "BookkeepingEntries",
 *   label = @Translation("Bookkeeping Entries", context = "Validation"),
 * )
 */
class BookkeepingEntriesConstraint extends Constraint {

  /**
   * Error when there are not enough entries.
   *
   * @var string
   */
  public $errorMessageCount = 'Transactions must have at least two entries.';

  /**
   * Error when transactions do not net to zero.
   *
   * @var string
   */
  public $errorMessageNotZero = 'Transactions must net to zero.';

}
